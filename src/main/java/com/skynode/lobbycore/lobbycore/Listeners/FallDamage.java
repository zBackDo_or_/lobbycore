package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

public class FallDamage implements Listener {

    @EventHandler
    public void onDamage(EntityDamageEvent e) {

        if (LobbyCore.getInstance().config.getConfiguration().getBoolean("DisableFallDamage") && e.getCause() == DamageCause.FALL) {
            e.setCancelled(true);
        }
    }
}
