package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerLeaveListener implements Listener {

    private String leavemessage = LobbyCore.getInstance().config.getConfiguration().getString("LeaveEvent.LeaveMessage");

    @EventHandler
    public void onLeave(PlayerQuitEvent e) {
        Player p = e.getPlayer();

        if (LobbyCore.getInstance().config.getConfiguration().getBoolean("LeaveEvent.EnableLeaveAnnouncer")) {
            e.setQuitMessage(ColorUtils.chat(leavemessage.replace("%player%", e.getPlayer().getName())));
        }
    }
}
