package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class TimeSystemListener implements Listener {
    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getSlot() == 3 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Day §6Time"))) {
            p.getLocation().getWorld().setTime(1000);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Time changed to §6Day!"));
        }
        if (e.getSlot() == 4 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Night §eTime"))) {
            p.getLocation().getWorld().setTime(13000);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Time changed to §eNight!"));
        }
        if (e.getSlot() == 5 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Sunrise §9Time"))) {
            p.getLocation().getWorld().setTime(23000);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Time changed to §9Sunrise!"));
        }
        if (e.getSlot() == 13 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Sunset §2Time"))) {
            p.getLocation().getWorld().setTime(12000);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Time changed to §2Sunset!"));
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Time§bSystem")) {
            p.closeInventory();
        }
    }
}