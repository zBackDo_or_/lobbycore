package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatEvent;

public class ChatListener implements Listener {

    @EventHandler
    public void onChat(PlayerChatEvent e) {
        Player p = e.getPlayer();
        if (LobbyCore.getInstance().config.getConfiguration().getBoolean("MuteChat")) {
            if (!p.isOp() || !p.hasPermission("lobbycore.mutechat.bypass")) {
                e.setCancelled(true);
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("MuteChat.TheChatIsMutedMessage")));
            }
        }
    }
}