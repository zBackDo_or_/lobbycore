package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class TeleportSystemListener implements Listener {

    @EventHandler
    public void onPlayerClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();
        if (e.getClickedInventory() != null) {
            if (e.getClickedInventory().getName().equals("§3Teleport §bSystem")) {
                if (p.hasPermission("lobbycore.tpgui")) {
                    if (e.getCurrentItem().getType() == Material.SKULL_ITEM) {
                        p.playSound(p.getEyeLocation(), Sound.ENDERMAN_TELEPORT, 1.0F, 1.0F);
                    }

                    int j = Bukkit.getOnlinePlayers().size();
                    Player[] playerarry = new Player[j];
                    Bukkit.getOnlinePlayers().toArray(playerarry);
                    String prefix = ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix"));

                    for (int i = 0; i < j; ++i) {
                        if (e.getCurrentItem().getItemMeta().getDisplayName() == playerarry[i].getDisplayName()) {
                            p.teleport(playerarry[i]);
                            Bukkit.broadcastMessage(prefix + "§3You have been teleported to §b" + playerarry[i].getName());
                            e.setCancelled(true);
                            return;
                        }
                    }
                }
                if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§cClose")) {
                    p.playSound(p.getEyeLocation(), Sound.CLICK, 1.0F, 1.0F);
                    p.closeInventory();
                }
            }
        }
    }
}