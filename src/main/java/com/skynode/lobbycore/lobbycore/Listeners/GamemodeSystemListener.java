package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

public class GamemodeSystemListener implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        if (e.getSlot() == 3 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Game§bMode §6Survival"))) {
            p.setGameMode(GameMode.SURVIVAL);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Successfully setted GameMode §6Survival!"));
        }
        if (e.getSlot() == 4 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Game§bMode §eCreative"))) {
            p.setGameMode(GameMode.CREATIVE);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Successfully setted GameMode §eCreative!"));
        }
        if (e.getSlot() == 5 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Game§bMode §9Adventure"))) {
            p.setGameMode(GameMode.ADVENTURE);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Successfully setted GameMode §9Adventure!"));
        }
        if (e.getSlot() == 13 && (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Game§bMode §2Spectator"))) {
            p.setGameMode(GameMode.SPECTATOR);
            p.closeInventory();
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Successfully setted GameMode §2Spectator!"));
        }
        if (e.getCurrentItem().getItemMeta().getDisplayName().equalsIgnoreCase("§3Game§bMode")) {
            p.closeInventory();
        }
    }
}
