package com.skynode.lobbycore.lobbycore.Listeners;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    private String joinmessage = LobbyCore.getInstance().config.getConfiguration().getString("JoinEvent.JoinMessage");


    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();

        if (LobbyCore.getInstance().config.getConfiguration().getBoolean("JoinEvent.EnableJoinAnnouncer")) {
            e.setJoinMessage(ColorUtils.chat(joinmessage.replace("%player%", e.getPlayer().getName().replaceAll("&", "§"))));
            Bukkit.getConsoleSender().sendMessage(ColorUtils.chat(joinmessage.replace("%player%", e.getPlayer().getName().replaceAll("&", "§"))));
        }
    }
}
