package com.skynode.lobbycore.lobbycore.API;

import com.skynode.lobbycore.lobbycore.Utils.ConfigSystem;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerConfig implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player player = e.getPlayer();
        ConfigSystem playerconf = new ConfigSystem("LobbyCore/Players", player.getUniqueId() + ".yml");
        playerconf.getConfiguration().set(player.getUniqueId() + ".Name", player.getName());
        playerconf.getConfiguration().set(player.getUniqueId() + ".World", player.getLocation().getWorld().getName());
        playerconf.getConfiguration().set(player.getUniqueId() + ".GameMode", player.getGameMode().name());
        playerconf.getConfiguration().set(player.getUniqueId() + ".Banned", player.isBanned());
        playerconf.getConfiguration().set(player.getUniqueId() + ".IsOp", player.isOp());
        playerconf.saveFile();
    }
}
