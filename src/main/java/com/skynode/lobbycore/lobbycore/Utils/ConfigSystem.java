
package com.skynode.lobbycore.lobbycore.Utils;

import java.io.File;
import java.io.IOException;
import org.bukkit.configuration.file.YamlConfiguration;

public class ConfigSystem {
    private File folder;
    private File file;
    private YamlConfiguration cfg;

    public ConfigSystem(String pluginName, String fileName) {
        this.folder = new File("plugins/" + pluginName + "/");
        this.file = new File("plugins/" + pluginName + "/" + fileName);
        this.setupFiles();
    }

    public ConfigSystem(File file) {
        this.folder = file.getParentFile();
        this.file = file;
        this.setupFiles();
    }

    public File getFolder() {
        return this.folder;
    }

    public File getFile() {
        return this.file;
    }

    public void saveFile() {
        try {
            this.cfg.save(this.file);
        } catch (IOException var2) {
        }

    }

    private void setupFiles() {
        if (!this.folder.exists()) {
            this.folder.mkdir();
        }

        if (!this.file.exists()) {
            try {
                this.file.createNewFile();
            } catch (IOException var2) {
            }
        }

        this.cfg = YamlConfiguration.loadConfiguration(this.file);
        this.saveFile();
    }

    public void addDefault(String path, Object value) {
        this.cfg.addDefault(path, value);
        this.cfg.options().copyDefaults(true);
    }

    public void setHeader(String value) {
        this.cfg.options().header(value);
        this.cfg.options().copyHeader(true);
    }

    public YamlConfiguration getConfiguration() {
        return this.cfg;
    }
}
