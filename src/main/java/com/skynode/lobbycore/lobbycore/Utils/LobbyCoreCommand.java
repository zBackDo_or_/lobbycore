package com.skynode.lobbycore.lobbycore.Utils;

import com.skynode.lobbycore.lobbycore.Commands.*;
import com.skynode.lobbycore.lobbycore.LobbyCore;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

public abstract class LobbyCoreCommand implements CommandExecutor {

    private final String commandName;
    private final String permission;
    private final boolean canConsoleUse;
    private static JavaPlugin plugin;

    public LobbyCoreCommand(String commandName, String permission, boolean canConsoleUse) {
        this.commandName = commandName;
        this.permission = permission;
        this.canConsoleUse = canConsoleUse;
        LobbyCore.getInstance().getCommand(commandName).setExecutor(this);
    }

    public abstract void execute(CommandSender sender, String[] arg);

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] arg) {
        if(!cmd.getLabel().equalsIgnoreCase(commandName))
            return true;
        if(!sender.hasPermission(permission)){
            sender.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("NoPermissionMessage")));
            return true;
        }
        if(!canConsoleUse && !(sender instanceof Player)){
            sender.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("NeedToBePlayerMessage")));
            return true;
        }
        execute(sender, arg);
        return true;
    }

    public static void registerCommands(JavaPlugin pl) {
        plugin = pl;
        new DayCommand();
        new NightCommand();
        new MainCommand();
        new SetSpawn();
        new Spawn();
        new TimeSystem();
        new TeleportSystem();
        new GamemodeSystem();
        new ClearChat();
        new Gmc();
        new Gms();
        new Gma();
        new Heal();
        new Rain();
        new Sun();
        new MuteChat();
        new UnMuteChat();
        new WorldSystem();
        new Sethome();
        new Home();
        new Delhome();
        new SetWarp();
        new Warp();
        new Warps();
        new DelWarp();
        new PermissionsSystem();
    }
}
