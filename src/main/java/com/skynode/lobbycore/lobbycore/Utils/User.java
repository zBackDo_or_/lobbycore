package com.skynode.lobbycore.lobbycore.Utils;

import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class User {

    private ConfigSystem homes = new ConfigSystem("LobbyCore", "Homes.yml");
    private ConfigSystem spawn = new ConfigSystem("LobbyCore", "Spawn.yml");

    // Home System

    public void setHome(Player player) {
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString() + ".X", player.getLocation().getX());
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString() + ".Y", player.getLocation().getY());
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString() + ".Z", player.getLocation().getZ());
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString() + ".Yaw", player.getLocation().getYaw());
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString() + ".Pitch", player.getLocation().getPitch());
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString() + ".World", player.getLocation().getWorld().getName());
        homes.saveFile();
    }

    public void goHome(Player player) {
        Location home = new Location(
                Bukkit.getWorld(homes.getConfiguration().getString("Homes." + player.getUniqueId().toString() + ".World"))
                , homes.getConfiguration().getDouble("Homes." + player.getUniqueId().toString() + ".X")
                , homes.getConfiguration().getDouble("Homes." + player.getUniqueId().toString() + ".Y")
                , homes.getConfiguration().getDouble("Homes." + player.getUniqueId().toString() + ".Z")
                , homes.getConfiguration().getLong("Homes." + player.getUniqueId().toString() + ".Yaw")
                , homes.getConfiguration().getLong("Homes." + player.getUniqueId().toString() + ".Pitch")
        );
        player.teleport(home);
    }

    public void delHome(Player player) {
        homes.getConfiguration().set("Homes." + player.getUniqueId().toString(), null);
        homes.saveFile();
    }

    public boolean homeIsNull(Player player) {
        if (homes.getConfiguration().getString("Homes." + player.getUniqueId()) == null) {
            return true;
        }
        else {
            return false;
        }
    }

    // Spawn System

    public void setSpawn(Player player) {
        spawn.getConfiguration().set("Spawn.World", player.getLocation().getWorld().getName());
        spawn.getConfiguration().set("Spawn.X", player.getLocation().getX());
        spawn.getConfiguration().set("Spawn.Y", player.getLocation().getY());
        spawn.getConfiguration().set("Spawn.Z", player.getLocation().getZ());
        spawn.getConfiguration().set("Spawn.Yaw", player.getLocation().getYaw());
        spawn.getConfiguration().set("Spawn.Pitch", player.getLocation().getPitch());
        spawn.saveFile();
    }

    public void goSpawn(Player player) {

        Location spawnloc = new Location(
                Bukkit.getWorld(spawn.getConfiguration().getString("Spawn.World"))
                , spawn.getConfiguration().getDouble("Spawn.X")
                , spawn.getConfiguration().getDouble("Spawn.Y")
                , spawn.getConfiguration().getDouble("Spawn.Z")
                , spawn.getConfiguration().getLong("Spawn.Yaw")
                , spawn.getConfiguration().getLong("Spawn.Pitch")
        );
        player.teleport(spawnloc);
    }

    public boolean spawnIsNull(Player player) {
        if (spawn.getConfiguration().getString("Spawn.") == null) {
            return true;
        } else {
            return false;
        }
    }

    // Title Sender

    public void sendTitle(Player player) {
        IChatBaseComponent chatTitle3 = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + "Type in chat the world name!" + "\",color:" + ChatColor.AQUA.name().toLowerCase() + "}");
        PacketPlayOutTitle title3 = new PacketPlayOutTitle(PacketPlayOutTitle.EnumTitleAction.TITLE, chatTitle3);
        PacketPlayOutTitle length3 = new PacketPlayOutTitle(5, 160, 5);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(title3);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(length3);
    }
}
