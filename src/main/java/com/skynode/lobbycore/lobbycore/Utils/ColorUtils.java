package com.skynode.lobbycore.lobbycore.Utils;

import net.md_5.bungee.api.ChatColor;

public class ColorUtils {

    public ColorUtils() {
    }

    public static String chat(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }
}