package com.skynode.lobbycore.lobbycore;

import com.skynode.lobbycore.lobbycore.API.PlayerConfig;
import com.skynode.lobbycore.lobbycore.Commands.PermissionsSystem;
import com.skynode.lobbycore.lobbycore.Commands.Warps;
import com.skynode.lobbycore.lobbycore.Commands.WorldSystem;
import com.skynode.lobbycore.lobbycore.Listeners.*;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.ConfigSystem;
import com.skynode.lobbycore.lobbycore.Utils.MetricsLite;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;

public final class LobbyCore extends JavaPlugin {

    // Plugin Instance
    private static LobbyCore plugin;

    // Configs List
    public ConfigSystem config = new ConfigSystem("LobbyCore", "Config.yml");
    public ConfigSystem messages = new ConfigSystem("LobbyCore", "Messages.yml");
    public ConfigSystem worlds = new ConfigSystem("LobbyCore", "Worlds.yml");
    public ConfigSystem warps = new ConfigSystem("LobbyCore", "Warps.yml");

    // Plugin Manager
    private PluginManager pm = Bukkit.getPluginManager();

    @Override
    public void onEnable() {

        plugin = this;

        // Check For Updates

        Bukkit.getConsoleSender().sendMessage("§a§m----------------------------------------");
        Bukkit.getConsoleSender().sendMessage(" ");
        Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §6Checking for updates...");
        Bukkit.getConsoleSender().sendMessage(" ");
        this.checkForUpdates();

        // Load methods

        this.createConfig();
        this.registerListener();
        LobbyCoreCommand.registerCommands(this);

        // Metrics

        try {
            MetricsLite metricsLite = new MetricsLite(this);
            Bukkit.getConsoleSender().sendMessage(" ");
            Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §2Successfully connected to bstats!");
        } catch (Exception e) {
            Bukkit.getConsoleSender().sendMessage(" ");
            Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §cCould not connect to bstats!");
        }

        // Startup message

        Bukkit.getConsoleSender().sendMessage(" ");
        Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §2§lPlugin §a§lEnabled!");
        Bukkit.getConsoleSender().sendMessage(" ");
        Bukkit.getConsoleSender().sendMessage("§a§m----------------------------------------");

    }

    @Override
    public void onDisable() {

        // Shutdown message

        plugin = null;

        Bukkit.getConsoleSender().sendMessage("§a§m----------------------------------------");
        Bukkit.getConsoleSender().sendMessage(" ");
        Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §2§lPlugin §c§lDisabled!");
        Bukkit.getConsoleSender().sendMessage(" ");
        Bukkit.getConsoleSender().sendMessage("§a§m----------------------------------------");
    }

    private void createConfig() {

        // Header
        messages.setHeader("Here you can edit all the LobbyCore Messages!!!");
        config.setHeader("Here you can setup the LobbyCore Plugin!");

        // Plugin Prefix

        messages.addDefault("PluginPrefix", "&3&lLobby&b&lCore >> ");

        // No Permission

        messages.addDefault("NoPermissionMessage", "&cYou don't have the permission!");

        // You have to be a player

        messages.addDefault("NeedToBePlayerMessage", "&cYou have to be a player!");

        // Player Offline

        messages.addDefault("PlayerOfflineMessage", "&cThis player is offline!");

        // AutoUpdater

        config.addDefault("AutoUpdater.EnableAutoUpdate", true);

        // Join and Leave listeners section

        config.addDefault("JoinEvent.EnableJoinAnnouncer", true);
        config.addDefault("JoinEvent.JoinMessage", "&3[&a+&3] &2%player%");
        config.addDefault("LeaveEvent.EnableLeaveAnnouncer", true);
        config.addDefault("LeaveEvent.LeaveMessage", "&3[&c-&3] &2%player%");

        // Spawn Section

        messages.addDefault("SpawnSystem.SpawnNotSettedYetMessage", "&cYou have to do first &2/setspawn!");

        messages.addDefault("SpawnSystem.TeleportToSpawnMessageLine1", "&3&m-----------------------------------------------------");
        messages.addDefault("SpawnSystem.TeleportToSpawnMessageLine2", " ");
        messages.addDefault("SpawnSystem.TeleportToSpawnMessageLine3", "&2You have been teleported to the spawn!");
        messages.addDefault("SpawnSystem.TeleportToSpawnMessageLine4", " ");
        messages.addDefault("SpawnSystem.TeleportToSpawnMessageLine5", "&3&m-----------------------------------------------------");

        messages.addDefault("SpawnSystem.SpawnSetMessageLine1", "&3&m-----------------------------------------------------");
        messages.addDefault("SpawnSystem.SpawnSetMessageLine2", " ");
        messages.addDefault("SpawnSystem.SpawnSetMessageLine3", "&2The spawn has been set!");
        messages.addDefault("SpawnSystem.SpawnSetMessageLine4", " ");
        messages.addDefault("SpawnSystem.SpawnSetMessageLine5", "&3&m-----------------------------------------------------");

        // Heal Section

        messages.addDefault("Heal.HealMessage", "&3You've been healed!");
        messages.addDefault("Heal.OnlySurivalOrAdventureModeMessage", "&cYou have to be in Survival mode or in Adventure mode!");
        messages.addDefault("Heal.CantHealPlayerMessage", "&cYou can't heal this player because he isn't in Survival or Adventure mode!");

        // MuteChat Section

        messages.addDefault("MuteChat.TheChatIsMutedMessage", "&cThe chat is muted!");
        messages.addDefault("MuteChat.ChatMuted", "&3You successfully muted the chat!");
        messages.addDefault("MuteChat.ChatUnMuted", "&3You successfully unmuted the chat!");
        // ChatClear Section

        messages.addDefault("ChatClear.ChatClearMessage", "&7&m---------------&2&lChat &e&lCleared&7&m---------------");

        // Fall Damage

        config.addDefault("DisableFallDamage", false);

        // Mute Chat

        config.addDefault("MuteChat", false);

        // Save the main config

        config.saveFile();
        messages.saveFile();
    }

    private void registerListener() {

        // Join And Leave Listeners

        pm.registerEvents(new PlayerJoinListener(), this);
        pm.registerEvents(new PlayerLeaveListener(), this);

        // GameMode System Listener

        pm.registerEvents(new GamemodeSystemListener(), this);

        // Teleport System Listener

        pm.registerEvents(new TeleportSystemListener(), this);

        // Fall Damage System

        pm.registerEvents(new FallDamage(), this);

        // Time System Listener

        pm.registerEvents(new TimeSystemListener(), this);

        // Player Config

        pm.registerEvents(new PlayerConfig(), this);

        // Chat Listener

        pm.registerEvents(new ChatListener(), this);

        // World System

        pm.registerEvents(new WorldSystem(), this);

        // Warp System

        pm.registerEvents(new Warps(), this);

        pm.registerEvents(new PermissionsSystem(), this);
    }

    public static LobbyCore getInstance() {
        return plugin;
    }

    private void saveAllFilesTask() {
        (new BukkitRunnable() {
            public void run() {
                config.saveFile();
                messages.saveFile();
                worlds.saveFile();
            }
        }).runTaskTimer(this, 6000L, 6000L);
    }

    private void checkForUpdates() {
        try {
            URL version = new URL("https://pastebin.com/raw/SDvVbrJm");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(version.openStream()));

            String inputLine;
            while ((inputLine = in.readLine()) != null)
                if (getDescription().getVersion().equals(inputLine)) {
                    Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §2You are using the latest version of §3Lobby§bCore!");
                } else if (config.getConfiguration().getBoolean("AutoUpdater.EnableAutoUpdate")) {
                    Bukkit.getConsoleSender().sendMessage("§3§lLobby§b§lCore >> §cYou are using an outdated version of §3Lobby§bCore§c! Installing the newer version...");
                    Bukkit.getConsoleSender().sendMessage(" ");
                    this.downloadUpdate();
                } else {
                    Bukkit.getConsoleSender().sendMessage("§3Lobby§bCore >> §cYou are using an outdated version of §3Lobby§bCore! §cPlease update me!");
                }
            in.close();
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage("§cSome errors are occured, cannot check for updates!");
        }
    }

    private void downloadUpdate() {

        try (BufferedInputStream inputStream = new BufferedInputStream(new URL("https://bitbucket.org/zBackDo_or_/lobbycore/downloads/LobbyCore-Latest.jar").openStream());
             FileOutputStream fileOS = new FileOutputStream(getDataFolder() + "/LobbyCore-Updated.jar")) {
            byte data[] = new byte[1024];
            int byteContent;
            while ((byteContent = inputStream.read(data, 0, 1024)) != -1) {
                fileOS.write(data, 0, byteContent);
            }
            Bukkit.getConsoleSender().sendMessage("§3Lobby§bCore >> §2Successfully downloaded the new version!");
            Bukkit.getConsoleSender().sendMessage(" ");
            Bukkit.getConsoleSender().sendMessage("§3Lobby§bCore >> §2A file named §3Lobby§bCore§3-Updated.jar §2has been generated into the §3Lobby§bCore §2folder!");
            Bukkit.getConsoleSender().sendMessage(" ");
            Bukkit.getConsoleSender().sendMessage("§3Lobby§bCore >> §2Now move it into the plugins folder and delete the old version file!");
        } catch (IOException e) {
            Bukkit.getConsoleSender().sendMessage("§3Lobby§bCore >> §cSome errors occured.. Please download the update manually!");
        }
    }
}