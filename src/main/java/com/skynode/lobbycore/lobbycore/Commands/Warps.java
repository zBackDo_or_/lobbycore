package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.Iterator;

public class Warps extends LobbyCoreCommand implements Listener {

    public Warps() {
        super("warps", "lobbycore.warps", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;
        if (LobbyCore.getInstance().warps.getConfiguration().getConfigurationSection("Warps") != null) {
            Inventory warps = Bukkit.getServer().createInventory(null, 54, "§3Warp§bList");
            Iterator iterator = LobbyCore.getInstance().warps.getConfiguration().getConfigurationSection("Warps").getKeys(false).iterator();

            while (iterator.hasNext()) {
                String key = (String) iterator.next();
                ItemStack item = new ItemStack(Material.STONE);
                ItemMeta meta = item.getItemMeta();
                meta.setDisplayName(key);
                meta.setLore(Arrays.asList(" ", "§3Click to teleport!"));
                item.setItemMeta(meta);
                warps.addItem(item);
            }

            p.openInventory(warps);
        } else {
            p.sendMessage("§cNo warp setted at moment!");
        }
    }

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        Player p = (Player) e.getWhoClicked();

        switch (e.getCurrentItem().getType()) {
            case STONE:
                Location warploc = new Location(
                        Bukkit.getWorld(LobbyCore.getInstance().warps.getConfiguration().getString("Warps." + e.getCurrentItem().getItemMeta().getDisplayName() + ".World"))
                        , LobbyCore.getInstance().warps.getConfiguration().getDouble("Warps." + e.getCurrentItem().getItemMeta().getDisplayName() + ".X")
                        , LobbyCore.getInstance().warps.getConfiguration().getDouble("Warps." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Y")
                        , LobbyCore.getInstance().warps.getConfiguration().getDouble("Warps." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Z")
                        , LobbyCore.getInstance().warps.getConfiguration().getLong("Warps." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Yaw")
                        , LobbyCore.getInstance().warps.getConfiguration().getLong("Warps." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Pitch")
                );
                p.teleport(warploc);
                p.sendMessage("§3Successfully teleported to the warp §e" + e.getCurrentItem().getItemMeta().getDisplayName() + "§3!");
        }
    }
}
