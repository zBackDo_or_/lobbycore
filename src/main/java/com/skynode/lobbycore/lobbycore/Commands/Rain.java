package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Rain extends LobbyCoreCommand {

    public Rain() {
        super("rain", "lobbycore.rain", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;
        if (!Bukkit.getWorld(player.getLocation().getWorld().getName()).hasStorm()) {
            Bukkit.getWorld(player.getLocation().getWorld().getName()).setStorm(true);
            player.sendMessage("§3Weather changed to §6Rain!");
        }
    }
}
