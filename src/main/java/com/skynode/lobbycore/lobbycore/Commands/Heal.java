package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Heal extends LobbyCoreCommand {

    public Heal() {
        super("heal", "lobbycore.heal", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;
        if (arg.length == 0) {
            if (p.getGameMode().equals(GameMode.SURVIVAL) || p.getGameMode().equals(GameMode.ADVENTURE)) {
                p.setHealth(20);
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix")) + ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("Heal.HealMessage")));
            } else {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("Heal.OnlySurvivalOrAdventureModeMessage")));
            }
        } else {
            Player target = Bukkit.getPlayerExact(arg[0]);
            if (target == null) {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PlayerOfflineMessage")));
            } else if (target.getGameMode().equals(GameMode.SURVIVAL) || target.getGameMode().equals(GameMode.ADVENTURE)) {
                p.sendMessage("§3You've healed §a" + arg[0]);
                target.setHealth(20);
                target.sendMessage("§a" + p.getName() + " §3healed you!");
            } else {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("Heal.CantHealPlayerMessage")));
            }
        }
    }
}