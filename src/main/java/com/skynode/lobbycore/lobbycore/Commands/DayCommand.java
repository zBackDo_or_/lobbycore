package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DayCommand extends LobbyCoreCommand {

    public DayCommand() {
        super("day", "lobbycore.day", false);
    }

    @Override
    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;
        player.getLocation().getWorld().setTime(1000);
        player.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Time set to §6Day!"));
    }
}