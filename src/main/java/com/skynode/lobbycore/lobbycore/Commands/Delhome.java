package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Delhome extends LobbyCoreCommand {

    public Delhome() {
        super("delhome", "lobbycore.delhome", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;
        User user = new User();
        user.delHome(p);
        p.sendMessage("§cSuccessfully deleted your home!");
    }
}
