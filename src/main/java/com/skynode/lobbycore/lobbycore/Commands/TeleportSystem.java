package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

public class TeleportSystem extends LobbyCoreCommand {

    public TeleportSystem() {
        super("tp", "lobbycore.teleportsystem", false);
    }

    public final void execute(final CommandSender sender, String[] arg) {
            Player player = (Player) sender;
                Inventory inv = Bukkit.createInventory((InventoryHolder) null, 36, "§3Teleport §bSystem");
                ItemStack close = new ItemStack(Material.WOOL, 1, (short) 14);
                ItemMeta closemeta = close.getItemMeta();
                closemeta.setDisplayName(ChatColor.DARK_RED + "Close");
                close.setItemMeta(closemeta);
                inv.setItem(31, close);
                int j = Bukkit.getOnlinePlayers().size();
                Player[] playerarry = new Player[j];
                Bukkit.getOnlinePlayers().toArray(playerarry);

                for (int i = 0; i < j; ++i) {
                    Player pa = playerarry[i];
                    ItemStack plbutton = new ItemStack(Material.SKULL_ITEM, 1, (short) SkullType.PLAYER.ordinal());
                    ItemMeta plbuttonmeta = (SkullMeta) plbutton.getItemMeta();
                    ((SkullMeta) plbuttonmeta).setOwner(playerarry[i].getName());
                    plbuttonmeta.setDisplayName("§3" + pa.getName());
                    plbutton.setItemMeta(plbuttonmeta);
                    inv.addItem(new ItemStack[]{plbutton});
                }

                player.openInventory(inv);
                player.playSound(player.getEyeLocation(), Sound.NOTE_PLING, 1.0F, 1.0F);
            }
        }