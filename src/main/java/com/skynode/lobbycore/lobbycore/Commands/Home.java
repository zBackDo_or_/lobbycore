package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Home extends LobbyCoreCommand {

    public Home() {
        super("home", "lobbycore.home", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        User user = new User();
        Player p = (Player) sender;
        if (user.homeIsNull(p)) {
            p.sendMessage("§cYou have not set up your house yet!");
        } else {
            user.goHome(p);
            p.sendMessage("§3Successfully teleported to your home!");
        }
    }
}
