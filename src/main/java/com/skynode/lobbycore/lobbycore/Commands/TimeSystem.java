package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.API.GUICreator;
import com.skynode.lobbycore.lobbycore.API.ItemCreator;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class TimeSystem extends LobbyCoreCommand {

    public TimeSystem() {
        super("time", "lobbycore.timesystem", false);
    }

    @Override
    public final void execute(final CommandSender sender, String[] arg) {
        Player p = (Player) sender;

        ItemStack day = new ItemCreator(Material.DIAMOND_BLOCK, 1, 0).setDisplayName("§3Day §6Time").build();
        ItemStack night = new ItemCreator(Material.COAL_BLOCK, 1, 0).setDisplayName("§3Night §eTime").build();
        ItemStack sunrise = new ItemCreator(Material.IRON_BLOCK, 1, 0).setDisplayName("§3Sunrise §9Time").build();
        ItemStack sunset = new ItemCreator(Material.GOLD_BLOCK, 1, 0).setDisplayName("§3Sunset §2Time").build();
        ItemStack glass = new ItemCreator(Material.STAINED_GLASS_PANE, 1, 0).setDisplayName("§3Time§bSystem").build();
        Inventory timegui = new GUICreator(p, 18, "§3Time §bMenu").setItem(3, day).setItem(4, night).setItem(5, sunrise).setItem(13, sunset).setItem(0, glass).setItem(1, glass).setItem(2, glass).setItem(6, glass).setItem(7, glass).setItem(8, glass).setItem(9, glass).setItem(10, glass).setItem(11, glass).setItem(12, glass).setItem(14, glass).setItem(15, glass).setItem(16, glass).setItem(17, glass).create();
        p.openInventory(timegui);
    }
}
