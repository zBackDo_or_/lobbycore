package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Sethome extends LobbyCoreCommand {

    public Sethome() {
        super("sethome", "lobbycore.sethome", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        User user = new User();
        Player p = (Player) sender;
        user.setHome(p);
        sender.sendMessage("§3Successfully setted your home!");
    }
}
