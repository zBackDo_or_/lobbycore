package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.command.CommandSender;

public class UnMuteChat extends LobbyCoreCommand {

    public UnMuteChat() {
        super("unmutechat", "lobbycore.unmutechat", true);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        LobbyCore.getInstance().config.getConfiguration().set("MuteChat", false);
        LobbyCore.getInstance().config.saveFile();
        sender.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("MuteChat.ChatUnMuted")));
    }
}
