package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.API.GUICreator;
import com.skynode.lobbycore.lobbycore.API.ItemCreator;
import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.User;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutTitle;
import org.bukkit.*;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;

public class WorldSystem extends LobbyCoreCommand implements Listener {

    public WorldSystem() {
        super("world", "lobbycore.world", false);
    }

    // ArrayLists
    private ArrayList<Player> flatlist = new ArrayList<Player>();
    private ArrayList<Player> normallist = new ArrayList<Player>();
    private ArrayList<Player> amplifiedlist = new ArrayList<Player>();

    private ArrayList<Player> netherlist = new ArrayList<Player>();
    private ArrayList<Player> endlist = new ArrayList<Player>();

    // HashMaps
    private HashMap<Player, String> playerworld = new HashMap<>();

    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;

        // ItemStacks
        ItemStack addworld = new ItemCreator(Material.EMERALD_BLOCK, 1, 0)
                .setDisplayName("§3Create a new world!")
                .build();
        ItemStack close = new ItemCreator(Material.BARRIER, 1, 0)
                .setDisplayName("§cClose")
                .build();
        ItemStack worldlist = new ItemCreator(Material.PAPER, 1, 0)
                .setDisplayName("§bWorld List")
                .build();
        ItemStack glass = new ItemCreator(Material.STAINED_GLASS_PANE, 1, 0)
                .setDisplayName("§3World§bSystem")
                .build();

        // Main Menu
        Inventory mainmenu = new GUICreator(p, 27, "§3World§bSystem")
                .setItem(0, glass)
                .setItem(10, close)
                .setItem(2, glass)
                .setItem(3, glass)
                .setItem(13, addworld)
                .setItem(5, glass)
                .setItem(6, glass)
                .setItem(16, worldlist)
                .setItem(0, glass)
                .setItem(1, glass)
                .setItem(2, glass)
                .setItem(3, glass)
                .setItem(4, glass)
                .setItem(5, glass)
                .setItem(6, glass)
                .setItem(7, glass)
                .setItem(8, glass)
                .setItem(9, glass)
                .setItem(11, glass)
                .setItem(12, glass)
                .setItem(14, glass)
                .setItem(15, glass)
                .setItem(17, glass)
                .setItem(18, glass)
                .setItem(19, glass)
                .setItem(20, glass)
                .setItem(21, glass)
                .setItem(22, glass)
                .setItem(23, glass)
                .setItem(24, glass)
                .setItem(25, glass)
                .setItem(26, glass)
                .create();
        p.openInventory(mainmenu);
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent e) {
        Player player = (Player) e.getWhoClicked();
        User user = new User();
        e.setCancelled(true);


        switch (e.getCurrentItem().getType()) {
            case EMERALD_BLOCK:
                player.closeInventory();
                Inventory worldtype = Bukkit.createInventory(null, 27, "§3World§bEnvironment");

                ItemStack normalworld = new ItemCreator(Material.GRASS, 1, 0)
                        .setDisplayName("§2§lNormal")
                        .build();
                ItemStack netherworld = new ItemCreator(Material.NETHERRACK, 1, 0)
                        .setDisplayName("§c§lNether")
                        .build();
                ItemStack endworld = new ItemCreator(Material.ENDER_PEARL, 1, 0)
                        .setDisplayName("§8§lEnd")
                        .build();
                ItemStack glass2 = new ItemCreator(Material.STAINED_GLASS_PANE, 1, 0)
                        .setDisplayName("§3World§bEnvironment")
                        .build();
                worldtype.setItem(10, normalworld);
                worldtype.setItem(13, netherworld);
                worldtype.setItem(16, endworld);
                worldtype.setItem(0, glass2);
                worldtype.setItem(1, glass2);
                worldtype.setItem(2, glass2);
                worldtype.setItem(3, glass2);
                worldtype.setItem(4, glass2);
                worldtype.setItem(5, glass2);
                worldtype.setItem(6, glass2);
                worldtype.setItem(7, glass2);
                worldtype.setItem(8, glass2);
                worldtype.setItem(9, glass2);
                worldtype.setItem(11, glass2);
                worldtype.setItem(12, glass2);
                worldtype.setItem(14, glass2);
                worldtype.setItem(15, glass2);
                worldtype.setItem(17, glass2);
                worldtype.setItem(18, glass2);
                worldtype.setItem(19, glass2);
                worldtype.setItem(20, glass2);
                worldtype.setItem(21, glass2);
                worldtype.setItem(22, glass2);
                worldtype.setItem(23, glass2);
                worldtype.setItem(24, glass2);
                worldtype.setItem(25, glass2);
                worldtype.setItem(26, glass2);
                player.openInventory(worldtype);
                break;
            case BARRIER:
                player.closeInventory();
                break;
            case PAPER:
                player.closeInventory();
                Inventory worlds = Bukkit.getServer().createInventory(null, 54, "§3World§bList");
                worlds.clear();
                player.openInventory(worlds);
                Iterator iterator = LobbyCore.getInstance().worlds.getConfiguration().getConfigurationSection("Worlds").getKeys(false).iterator();
                ItemStack chiudi = new ItemCreator(Material.BARRIER, 1, 0)
                        .setDisplayName("§cClose")
                        .build();
                while (iterator.hasNext()) {
                    String key = (String) iterator.next();
                    ItemStack item = new ItemStack(Material.DIRT);
                    ItemMeta meta = item.getItemMeta();
                    meta.setDisplayName(key);
                    meta.setLore(Arrays.asList(" ", "§3Spawn Location", " ", "§ax: §7" + LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + key.toLowerCase() + ".Spawn.x"), "§ay: §7" + LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + key.toLowerCase() + ".Spawn.y"), "§az: §7" + LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + key.toLowerCase() + ".Spawn.z"), " ", "§3World Flags", " ", "§aPvP: §f" + LobbyCore.getInstance().worlds.getConfiguration().getBoolean("Worlds." + key.toLowerCase() + ".PvP"), "§aAnimals: §f" + LobbyCore.getInstance().worlds.getConfiguration().getBoolean("Worlds." + key.toLowerCase() + ".Animals"), "§aMonsters: §f" + LobbyCore.getInstance().worlds.getConfiguration().getBoolean("Worlds." + key.toLowerCase() + ".Monsters"), "§aInvincible: §f" + LobbyCore.getInstance().worlds.getConfiguration().getBoolean("Worlds." + key.toLowerCase() + ".Invincible"), " ", "§3Left click to teleport!", " ", "§eRight click to show options!"));
                    item.setItemMeta(meta);
                    worlds.addItem(item);
                }
                worlds.setItem(49, chiudi);
                break;
            case DIRT:
                if (e.getClick() == ClickType.LEFT) {
                    player.closeInventory();
                    new WorldCreator(e.getCurrentItem().getItemMeta().getDisplayName()).createWorld();
                    Location loc = new Location(LobbyCore.getInstance().getServer().getWorld(e.getCurrentItem().getItemMeta().getDisplayName())
                            , LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Spawn.x")
                            , LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Spawn.y")
                            , LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + e.getCurrentItem().getItemMeta().getDisplayName() + ".Spawn.z"));
                    player.teleport(loc);
                    player.sendMessage("§3Successfully teleported to the world §e" + e.getCurrentItem().getItemMeta().getDisplayName());
                } else if (e.getClick() == ClickType.RIGHT) {
                    playerworld.put(player, e.getCurrentItem().getItemMeta().getDisplayName());
                    player.closeInventory();
                    Inventory worldsettings = Bukkit.getServer().createInventory(null, 27, "§3World§bSettings");
                    ItemStack deleteworld = new ItemCreator(Material.REDSTONE_BLOCK, 1, 0)
                            .setDisplayName("§cDelete the world!")
                            .build();
                    ItemStack setspawn = new ItemCreator(Material.SIGN, 1, 0)
                            .setDisplayName("§3Set the spawn!")
                            .build();
                    ItemStack tptospawn = new ItemCreator(Material.SLIME_BALL, 1, 0)
                            .setDisplayName("§eTeleport to world spawn!")
                            .build();
                    ItemStack glass = new ItemCreator(Material.STAINED_GLASS_PANE, 1, 0)
                            .setDisplayName("§3World§bSettings")
                            .build();
                    worldsettings.setItem(10, deleteworld);
                    worldsettings.setItem(13, setspawn);
                    worldsettings.setItem(16, tptospawn);
                    worldsettings.setItem(0, glass);
                    worldsettings.setItem(1, glass);
                    worldsettings.setItem(2, glass);
                    worldsettings.setItem(3, glass);
                    worldsettings.setItem(4, glass);
                    worldsettings.setItem(5, glass);
                    worldsettings.setItem(6, glass);
                    worldsettings.setItem(7, glass);
                    worldsettings.setItem(8, glass);
                    worldsettings.setItem(9, glass);
                    worldsettings.setItem(11, glass);
                    worldsettings.setItem(12, glass);
                    worldsettings.setItem(14, glass);
                    worldsettings.setItem(15, glass);
                    worldsettings.setItem(17, glass);
                    worldsettings.setItem(18, glass);
                    worldsettings.setItem(19, glass);
                    worldsettings.setItem(20, glass);
                    worldsettings.setItem(21, glass);
                    worldsettings.setItem(22, glass);
                    worldsettings.setItem(23, glass);
                    worldsettings.setItem(24, glass);
                    worldsettings.setItem(25, glass);
                    worldsettings.setItem(26, glass);
                    player.openInventory(worldsettings);
                }
                break;
            case GRASS:
                player.closeInventory();
                Inventory normaltype = Bukkit.createInventory(null, 27, "§3World§bType");
                ItemStack normal = new ItemCreator(Material.GOLD_BLOCK)
                        .setDisplayName("§3§lNormal")
                        .build();
                ItemStack flat = new ItemCreator(Material.IRON_BLOCK)
                        .setDisplayName("§6§lFlatWorld")
                        .build();
                ItemStack amplified = new ItemCreator(Material.DIAMOND_BLOCK)
                        .setDisplayName("§5§lAmplified")
                        .build();
                ItemStack glass = new ItemCreator(Material.STAINED_GLASS_PANE)
                        .setDisplayName("§3World§bType")
                        .build();
                normaltype.setItem(10, flat);
                normaltype.setItem(13, normal);
                normaltype.setItem(16, amplified);
                normaltype.setItem(0, glass);
                normaltype.setItem(1, glass);
                normaltype.setItem(2, glass);
                normaltype.setItem(3, glass);
                normaltype.setItem(4, glass);
                normaltype.setItem(5, glass);
                normaltype.setItem(6, glass);
                normaltype.setItem(7, glass);
                normaltype.setItem(8, glass);
                normaltype.setItem(9, glass);
                normaltype.setItem(11, glass);
                normaltype.setItem(12, glass);
                normaltype.setItem(14, glass);
                normaltype.setItem(15, glass);
                normaltype.setItem(17, glass);
                normaltype.setItem(18, glass);
                normaltype.setItem(19, glass);
                normaltype.setItem(20, glass);
                normaltype.setItem(21, glass);
                normaltype.setItem(22, glass);
                normaltype.setItem(23, glass);
                normaltype.setItem(24, glass);
                normaltype.setItem(25, glass);
                normaltype.setItem(26, glass);
                player.openInventory(normaltype);
                break;
            case IRON_BLOCK:
                player.closeInventory();
                flatlist.add(player);
                user.sendTitle(player);
                break;
            case GOLD_BLOCK:
                player.closeInventory();
                normallist.add(player);
                user.sendTitle(player);
                break;
            case DIAMOND_BLOCK:
                player.closeInventory();
                amplifiedlist.add(player);
                user.sendTitle(player);
                break;
            case NETHERRACK:
                player.closeInventory();
                netherlist.add(player);
                user.sendTitle(player);
                break;
            case ENDER_PEARL:
                player.closeInventory();
                endlist.add(player);
                user.sendTitle(player);
                break;
            case REDSTONE_BLOCK:
                player.closeInventory();
                String value = playerworld.get(player);
                World world = Bukkit.getServer().getWorld(value);
                new WorldCreator(value).createWorld();
                LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + world.getName(), null);
                Bukkit.unloadWorld(world, true);
                LobbyCore.getInstance().worlds.saveFile();
                player.sendMessage("§3Successfully deleted the world §e" + world.getName() + "§3!");
                break;
            case SIGN:
                player.closeInventory();
                String value2 = playerworld.get(player);
                World world1 = Bukkit.getServer().getWorld(value2);
                LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + world1.getName().toLowerCase() + ".Spawn.x", player.getLocation().getBlockX());
                LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + world1.getName().toLowerCase() + ".Spawn.y", player.getLocation().getBlockY());
                LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + world1.getName().toLowerCase() + ".Spawn.z", player.getLocation().getBlockZ());
                LobbyCore.getInstance().worlds.saveFile();
                player.sendMessage("§3Successfully updated the world spawnpoint!");
                break;
            case SLIME_BALL:
                player.closeInventory();
                String value3 = playerworld.get(player);
                World world2 = Bukkit.getServer().getWorld(value3);
                new WorldCreator(value3).createWorld();
                Location loc = new Location(LobbyCore.getInstance().getServer().getWorld(world2.getName().toLowerCase())
                        , LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + world2.getName().toLowerCase() + ".Spawn.x")
                        , LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + world2.getName().toLowerCase() + ".Spawn.y")
                        , LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + world2.getName().toLowerCase() + ".Spawn.z"));
                player.teleport(loc);
                player.sendMessage("§3Successfully teleported to the world spawn!");
                break;
        }
    }

    @EventHandler
    public void onChat(PlayerChatEvent e) {
        Player p = e.getPlayer();
        if (flatlist.contains(p)) {
            WorldCreator normalworld = WorldCreator.name(e.getMessage()).environment(World.Environment.NORMAL).type(WorldType.FLAT);
            e.setCancelled(true);
            p.sendMessage("§3Creating the world §e" + normalworld.name() + "§3...");
            Bukkit.createWorld(normalworld);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.y", Bukkit.getWorld(normalworld.name()).getHighestBlockYAt(LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x"), LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x")));
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.z", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Monsters", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Animals", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".PvP", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".invincible", false);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Environment", "Normal");
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Type", "FlatWorld");
            LobbyCore.getInstance().worlds.saveFile();
            p.sendMessage("§3Successfully created the world §e" + normalworld.name() + "§3!");
            flatlist.remove(p);
        }
        if (normallist.contains(p)) {
            WorldCreator normalworld = WorldCreator.name(e.getMessage()).environment(World.Environment.NORMAL).type(WorldType.NORMAL);
            e.setCancelled(true);
            p.sendMessage("§3Creating the world §e" + normalworld.name() + "§3...");
            Bukkit.createWorld(normalworld);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.y", Bukkit.getWorld(normalworld.name()).getHighestBlockYAt(LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x"), LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x")));
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.z", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Monsters", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Animals", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".PvP", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".invincible", false);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Environment", "Normal");
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Type", "Normal");
            LobbyCore.getInstance().worlds.saveFile();
            p.sendMessage("§3Successfully created the world §e" + normalworld.name() + "§3!");
            normallist.remove(p);
        }
        if (amplifiedlist.contains(p)) {
            WorldCreator normalworld = WorldCreator.name(e.getMessage()).environment(World.Environment.NORMAL).type(WorldType.AMPLIFIED);
            e.setCancelled(true);
            p.sendMessage("§3Creating the world §e" + normalworld.name() + "§3...");
            Bukkit.createWorld(normalworld);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.y", Bukkit.getWorld(normalworld.name()).getHighestBlockYAt(LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x"), LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + normalworld.name().toLowerCase() + ".Spawn.x")));
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Spawn.z", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Monsters", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Animals", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".PvP", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".invincible", false);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Environment", "Normal");
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + normalworld.name().toLowerCase() + ".Type", "Amplified");
            LobbyCore.getInstance().worlds.saveFile();
            p.sendMessage("§3Successfully created the world §e" + normalworld.name() + "§3!");
            amplifiedlist.remove(p);
        }
        if (netherlist.contains(p)) {
            WorldCreator netherworld = WorldCreator.name(e.getMessage()).environment(World.Environment.NETHER);
            e.setCancelled(true);
            p.sendMessage("§3Creating the world §e" + netherworld.name() + "§3...");
            Bukkit.createWorld(netherworld);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".Spawn.x", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".Spawn.y", Bukkit.getWorld(netherworld.name()).getHighestBlockYAt(LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + netherworld.name().toLowerCase() + ".Spawn.x"), LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + netherworld.name().toLowerCase() + ".Spawn.x")));
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".Spawn.z", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".Monsters", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".Animals", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".PvP", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".invincible", false);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + netherworld.name().toLowerCase() + ".Environment", "Nether");
            LobbyCore.getInstance().worlds.saveFile();
            p.sendMessage("§3Successfully created the world §e" + netherworld.name() + "§3!");
            netherlist.remove(p);
        }
        if (endlist.contains(p)) {
            WorldCreator endworld = WorldCreator.name(e.getMessage()).environment(World.Environment.THE_END);
            e.setCancelled(true);
            p.sendMessage("§3Creating the world §e" + endworld.name() + "§3...");
            Bukkit.createWorld(endworld);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".Spawn.x", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".Spawn.y", Bukkit.getWorld(endworld.name()).getHighestBlockYAt(LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + endworld.name().toLowerCase() + ".Spawn.x"), LobbyCore.getInstance().worlds.getConfiguration().getInt("Worlds." + endworld.name().toLowerCase() + ".Spawn.x")));
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".Spawn.z", 0);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".Monsters", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".Animals", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".PvP", true);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".invincible", false);
            LobbyCore.getInstance().worlds.getConfiguration().set("Worlds." + endworld.name().toLowerCase() + ".Environment", "End");
            LobbyCore.getInstance().worlds.saveFile();
            p.sendMessage("§3Successfully created the world §e" + endworld.name() + "§3!");
            endlist.remove(p);
        }
    }

    @EventHandler
    public void onQuit(PlayerQuitEvent e) {
        Player p = e.getPlayer();
        if (flatlist.contains(p)) {
            flatlist.remove(p);
        }
        if (normallist.contains(p)) {
            normallist.remove(p);
        }
        if (amplifiedlist.contains(p)) {
            amplifiedlist.remove(p);
        }
        if (netherlist.contains(p)) {
            netherlist.remove(p);
        }
        if (endlist.contains(p)) {
            endlist.remove(p);
        }
    }
}