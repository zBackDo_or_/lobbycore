package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Warp extends LobbyCoreCommand {

    public Warp() {
        super("warp", "lobbycore.warp", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;
        if (arg.length == 1) {
            if (LobbyCore.getInstance().warps.getConfiguration().getString("Warps." + arg[0]) != null) {

                Location warploc = new Location(
                        Bukkit.getWorld(LobbyCore.getInstance().warps.getConfiguration().getString("Warps." + arg[0] + ".World"))
                        , LobbyCore.getInstance().warps.getConfiguration().getDouble("Warps." + arg[0] + ".X")
                        , LobbyCore.getInstance().warps.getConfiguration().getDouble("Warps." + arg[0] + ".Y")
                        , LobbyCore.getInstance().warps.getConfiguration().getDouble("Warps." + arg[0] + ".Z")
                        , LobbyCore.getInstance().warps.getConfiguration().getLong("Warps." + arg[0] + ".Yaw")
                        , LobbyCore.getInstance().warps.getConfiguration().getLong("Warps." + arg[0] + ".Pitch")
                );
                p.teleport(warploc);
                p.sendMessage("§3Successfully teleported to the warp §e" + arg[0] + "§3!");
            } else {
                p.sendMessage("§cWarp not found!");
            }
        } else {
            p.sendMessage("§3Usage: /warp <Name>");
        }
    }
}
