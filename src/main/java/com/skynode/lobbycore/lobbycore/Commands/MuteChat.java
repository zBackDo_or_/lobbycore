package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.command.CommandSender;

public class MuteChat extends LobbyCoreCommand {

    public MuteChat() {
        super("mutechat", "lobbycore.mutechat", true);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        LobbyCore.getInstance().config.getConfiguration().set("MuteChat", true);
        LobbyCore.getInstance().config.saveFile();
        sender.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("MuteChat.ChatMuted")));
    }
}
