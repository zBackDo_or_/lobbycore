package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.permissions.PermissionAttachment;

import java.util.HashMap;
import java.util.UUID;

public class PermissionsSystem extends LobbyCoreCommand implements Listener {

    private HashMap<UUID, PermissionAttachment> perms = new HashMap<>();

    public PermissionsSystem() {
        super("lbp", "lobbycore.permissions", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;

        if (arg == null) {
            p.sendMessage("§3Usage: /lbp add <Permission>");

        } else if (arg[0].equalsIgnoreCase("add")){
            PermissionAttachment playerperm = perms.get(p.getUniqueId());
            playerperm.setPermission(arg[1], true);
            p.sendMessage("§3Successfully added to you the permission §e" + arg[1]);
        }
    }

    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        Player p = e.getPlayer();
        PermissionAttachment attachment = p.addAttachment(LobbyCore.getInstance());
        perms.put(p.getUniqueId(), attachment);
    }
}
