package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.command.CommandSender;

public class MainCommand extends LobbyCoreCommand {

    public MainCommand() {
        super("lobbycore", "lobbycore.admin", true);
    }

    @Override
    public final void execute(final CommandSender sender, final String[] arg) {
        sender.sendMessage("§7§m---------------§3§lLobby§b§lCore§7§m---------------");
        sender.sendMessage(" ");
        sender.sendMessage("§3Version: §e" + LobbyCore.getInstance().getDescription().getVersion());
        sender.sendMessage("§3Authors: §3" + LobbyCore.getInstance().getDescription().getAuthors());
        sender.sendMessage(" ");
        sender.sendMessage("§7§m---------------§3§lLobby§b§lCore§7§m---------------");
    }
}