package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.API.GUICreator;
import com.skynode.lobbycore.lobbycore.API.ItemCreator;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Material;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GamemodeSystem extends LobbyCoreCommand {

    public GamemodeSystem() {
        super("gamemode", "lobbycore.gamemode", false);
    }

    @Override
    public final void execute(final CommandSender sender, final String[] arg) {

        Player p = (Player) sender;
        ItemStack gms = new ItemCreator(Material.GRASS, 1, 0).setDisplayName("§3Game§bMode §6Survival").build();
        ItemStack gmc = new ItemCreator(Material.BRICK, 1, 0).setDisplayName("§3Game§bMode §eCreative").build();
        ItemStack gmadventure = new ItemCreator(Material.DIAMOND_SWORD, 1, 0).setDisplayName("§3Game§bMode §9Adventure").build();
        ItemStack gmspectator = new ItemCreator(Material.FEATHER, 1, 0).setDisplayName("§3Game§bMode §2Spectator").build();
        ItemStack glass = new ItemCreator(Material.STAINED_GLASS_PANE, 1, 0).setDisplayName("§3Game§bMode").build();
        Inventory gmgui = new GUICreator(p, 18, "§3GameMode §bMenu").setItem(3, gms).setItem(4, gmc).setItem(5, gmadventure).setItem(13, gmspectator).setItem(0, glass).setItem(1, glass).setItem(2, glass).setItem(6, glass).setItem(7, glass).setItem(8, glass).setItem(9, glass).setItem(10, glass).setItem(11, glass).setItem(12, glass).setItem(14, glass).setItem(15, glass).setItem(16, glass).setItem(17, glass).create();
        p.openInventory(gmgui);
    }
}
