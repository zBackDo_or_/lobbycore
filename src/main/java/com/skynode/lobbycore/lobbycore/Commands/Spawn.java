package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Spawn extends LobbyCoreCommand {

    public Spawn() {
        super("spawn", "lobbycore.spawn", false);
    }

    @Override
    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;
        User user = new User();

        if (user.spawnIsNull(p)) {
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnNotSettedYetMessage")));
        } else {

            user.goSpawn(p);

            if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine1") != null) {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine1")));
            }
            if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine2") != null) {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine2")));
            }
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix")) + ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine3")));
            if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine4") != null) {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine4")));
            }
            if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine5") != null) {
                p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.TeleportToSpawnMessageLine5")));
            }
        }
    }
}