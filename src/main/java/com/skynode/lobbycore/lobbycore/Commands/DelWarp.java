package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class DelWarp extends LobbyCoreCommand {

    public DelWarp() {
        super("delwarp", "lobbycore.delwarp", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;
        if (arg.length == 1) {
            if (LobbyCore.getInstance().warps.getConfiguration().contains("Warps." + arg[0])) {
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0], null);
                LobbyCore.getInstance().warps.saveFile();

                player.sendMessage("§3Successfully removed the warp §e" + arg[0] + "§3!");
            } else {
                player.sendMessage("§cWarp not exists!");
            }
        } else {
            player.sendMessage("§3Usage: /delwarp <Name>");
        }
    }
}
