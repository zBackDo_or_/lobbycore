package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.User;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetSpawn extends LobbyCoreCommand {

    public SetSpawn() {
        super("setspawn", "lobbycore.setspawn", false);
    }

    @Override
    public final void execute(final CommandSender sender, final String[] arg) {
        Player p = (Player) sender;

        User user = new User();
        user.setSpawn(p);

        if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine1") != null) {
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine1")));
        }

        if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine2") != null) {
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine2")));
        }

        p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix")) + ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine3")));

        if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine4") != null) {
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine4")));
        }

        if (LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine5") != null) {
            p.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("SpawnSystem.SpawnSetMessageLine5")));
        }
    }
}