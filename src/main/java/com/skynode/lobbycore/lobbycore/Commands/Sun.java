package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Sun extends LobbyCoreCommand {

    public Sun() {
        super("sun", "lobbycore.sun", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;
        if (Bukkit.getWorld(player.getLocation().getWorld().getName()).hasStorm()) {
            Bukkit.getWorld(player.getLocation().getWorld().getName()).setStorm(false);
            player.sendMessage("§3Weather changed to §6Sun!");
        }
    }
}
