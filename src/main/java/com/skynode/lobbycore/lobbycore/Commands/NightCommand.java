package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class NightCommand extends LobbyCoreCommand {

    public NightCommand() {
        super("night", "lobbycore.night", false);
    }

    @Override
    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;
        player.getLocation().getWorld().setTime(13000);
        player.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3Time changed to §6Night!"));
    }
}