package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.ColorUtils;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Gms extends LobbyCoreCommand {

    public Gms() {
        super("gms", "lobbycore.gms", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;

        if (arg.length == 0) {
            player.setGameMode(GameMode.SURVIVAL);
            player.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PluginPrefix") + "§3GameMode changed to §6Survival!"));
        } else {
            Player target = Bukkit.getPlayerExact(arg[0]);
            if (target == null) {
                player.sendMessage(ColorUtils.chat(LobbyCore.getInstance().messages.getConfiguration().getString("PlayerOfflineMessage")));
            } else {
                player.sendMessage("§3GameMode setted to survival for §e" + arg[0]);
                target.setGameMode(GameMode.SURVIVAL);
            }
        }
    }
}
