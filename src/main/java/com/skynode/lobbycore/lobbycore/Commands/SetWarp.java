package com.skynode.lobbycore.lobbycore.Commands;

import com.skynode.lobbycore.lobbycore.LobbyCore;
import com.skynode.lobbycore.lobbycore.Utils.LobbyCoreCommand;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SetWarp extends LobbyCoreCommand {

    public SetWarp() {
        super("setwarp", "lobbycore.setwarp", false);
    }

    public final void execute(final CommandSender sender, final String[] arg) {
        Player player = (Player) sender;
        if (arg.length == 1) {
            if (!LobbyCore.getInstance().warps.getConfiguration().contains("Warps." + arg[0])) {
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0] + ".World", player.getLocation().getWorld().getName());
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0] + ".X", player.getLocation().getX());
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0] + ".Y", player.getLocation().getY());
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0] + ".Z", player.getLocation().getZ());
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0] + ".Yaw", player.getLocation().getYaw());
                LobbyCore.getInstance().warps.getConfiguration().set("Warps." + arg[0] + ".Pitch", player.getLocation().getPitch());
                LobbyCore.getInstance().warps.saveFile();

                player.sendMessage("§3Successfully setted the warp §e" + arg[0] + "§3!");
            } else {
                player.sendMessage("§cWarp Already Exists!");
            }
        } else {
            player.sendMessage("§3Usage: /setwarp <Name>");
        }
    }
}
